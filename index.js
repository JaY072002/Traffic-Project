// for traffic signal 1 

function trafficSignal1() {
    let redsignal1 = document.getElementById("red1");
    let yellosignal1 = document.getElementById("yello1");
    let greensignal1 = document.getElementById("green1")

    for (let i = 30; i >= 0; i--) {
        setTimeout(() => {
            redsignal1.innerHTML = i
        }, 1000 * (30 - i))
        redsignal1.style.boxShadow = "0 0 25px 15px green";
        yellosignal1.style.opacity = "0.3"
        greensignal1.style.opacity = "0.3"
    }

    setTimeout(() => {
        for (let i = 5; i >= 0; i--) {
            setTimeout(() => {
                yellosignal1.innerHTML = i;
            }, 1000 * (5 - i))

            redsignal1.style.color = "transparent";
            yellosignal1.style.boxShadow = "0 0 25px 15px yellow"
            yellosignal1.style.opacity = "1"
            redsignal1.style.opacity = "0.3"
            greensignal1.style.opacity = "0.3"
        }
    }, 30000);

    setTimeout(() => {
        for (let i = 105; i >= 0; i--) {
            setTimeout(() => {
                greensignal1.innerHTML = i
            }, 1000 * (105 - i))

            yellosignal1.style.color = "transparent"
            greensignal1.style.boxShadow = "0 0 25px 15px red"
            greensignal1.style.opacity = "1"
            redsignal1.style.opacity = "0.3"
            yellosignal1.style.opacity = "0.3"
        }
    }, 35000);
}

// for traffic signal 2 
function trafficSignal2() {
    let redsignal2 = document.getElementById("red2")
    let yellosignal2 = document.getElementById("yello2")
    let greensignal2 = document.getElementById("green2")

    for(let i = 35; i>=0; i--) {
        setTimeout(() => {
            greensignal2.innerHTML = i
        }, 1000 * (35 - i));

        greensignal2.style.boxShadow = "0 0 25px 10px red"
        redsignal2.style.opacity = "0.3"
        yellosignal2.style.opacity = "0.3"
    }
    setTimeout(() => {
        for(let i = 30; i>=0; i--) {
            setTimeout(() => {
                redsignal2.innerHTML = i
            }, 1000 * (30 - i));
        }

        greensignal2.style.color = "transparent";
        redsignal2.style.boxShadow = "0 0 25px 10px green"
        redsignal2.style.opacity = "1"
        greensignal2.style.opacity = "0.3"
        yellosignal2.style.opacity = "0.3"
    }, 35000);

    setTimeout(() => {
        for (let i = 5; i >= 0; i--) {
            setTimeout(() => {
                yellosignal2.innerHTML = i
            }, 1000 * (5 - i))

            redsignal2.style.color = "transparent"
            yellosignal2.style.boxShadow = "0 0 25px 10px yellow"
            yellosignal2.style.opacity = "1"
            redsignal2.style.opacity = "0.3"
            greensignal2.style.opacity = "0.3"
        }
    }, 65000);

    setTimeout(() => {
        for(let i = 105; i>=0; i--) {
            setTimeout(() => {
                greensignal2.innerHTML = i
            }, 1000 * (105 - i));
        }
        yellosignal2.style.color = "transparent"
        greensignal2.style.color = "white"
        greensignal2.style.boxShadow = "0 0 25px 10px red"
        greensignal2.style.opacity = "1"
        redsignal2.style.opacity = "0.3"
        yellosignal2.style.opacity = "0.3"
    }, 70000);
}


// for traffic signal 3 

function trafficSignal3() {
    let redsignal3 = document.getElementById("red3")
    let greensignal3 = document.getElementById("green3")
    let yellosignal3 = document.getElementById("yello3")

    for(let i = 70; i>=0; i--) {
        setTimeout(() => {
            greensignal3.innerHTML = i
        }, 1000 * (70 - i));
        greensignal3.style.boxShadow = "0 0 25px 10px red"
        redsignal3.style.opacity = "0.3"
        yellosignal3.style.opacity = "0.3"

    }

    setTimeout(() => {
        for(let i = 30; i>=0; i--) {
            setTimeout(() => {
                redsignal3.innerHTML = i
            }, 1000 * (30-i));
        }
        greensignal3.style.color = "transparent"
        redsignal3.style.boxShadow = "0 0 25px 10px green"
        redsignal3.style.opacity = "1"
        greensignal3.style.opacity = "0.3"
        yellosignal3.style.opacity = "0.3"

    }, 70000);

    setTimeout(() => {
        for(let i = 5; i>=0; i--) {
            setTimeout(() => {
                yellosignal3.innerHTML = i
            }, 1000 * (5-i));
        }
        redsignal3.style.color = "transparent"
        yellosignal3.style.boxShadow = "0 0 25px 10px yellow"
        yellosignal3.style.opacity = "1"
        greensignal3.style.opacity = "0.3"
        redsignal3.style.opacity = "0.3"
    }, 100000);

    setTimeout(() => {
        for(let i = 105; i>=0; i--) {
            setTimeout(() => {
                greensignal3.innerHTML = i
            }, 1000 * (105-i));
        }
        yellosignal3.style.color = "transparent"
        greensignal3.style.color = "white"
        greensignal3.style.boxShadow = "0 0 25px 10px red"
        greensignal3.style.opacity = "1"
        redsignal3.style.opacity = "0.3"
        yellosignal3.style.opacity = "0.3"
    }, 105000);
}

// for traffic light 4 

function trafficSignal4() {
    let redsignal4 = document.getElementById("red4")
    let greensignal4 = document.getElementById("green4")
    let yellosignal4 = document.getElementById("yello4")

    for(let i = 105; i>=0; i--) {
        setTimeout(() => {
            greensignal4.innerHTML = i
        }, 1000 * (105 - i));
        greensignal4.style.boxShadow = "0 0 25px 10px red"
        redsignal4.style.opacity = "0.3"
        yellosignal4.style.opacity = "0.3"
    }

    setTimeout(() => {
        for(let i = 30; i>=0; i--) {
            setTimeout(() => {
                redsignal4.innerHTML = i
            }, 1000 * (30-i));
        }
        greensignal4.style.color = "transparent"
        redsignal4.style.boxShadow = "0 0 25px 10px green"
        redsignal4.style.opacity = "1"
        yellosignal4.style.opacity = "0.3"
        greensignal4.style.opacity = "0.3"
    }, 105000);

    setTimeout(() => {
        for(let i = 5; i>=0; i--) {
            setTimeout(() => {
                yellosignal4.innerHTML = i
            }, 1000 * (5-i));
        }
        redsignal4.style.color = "transparent"
        yellosignal4.style.boxShadow = "0 0 25px 10px yellow"
        yellosignal4.style.opacity = "1"
        redsignal4.style.opacity = "0.3"
        greensignal4.style.opacity = "0.3"
    }, 135000);

    setTimeout(() => {
        for(let i = 105; i>=0; i--) {
            setTimeout(() => {
                greensignal4.innerHTML = i
            }, 1000 * (105-i));
        }
        yellosignal4.style.color = "transparent"
        greensignal4.style.color = "white"
        greensignal4.style.boxShadow = "0 0 25px 10px red"
        greensignal4.style.opacity = "1"
        redsignal4.style.opacity = "0.3"
        yellosignal4.style.opacity = "0.3"
    }, 140000);
}

trafficSignal1()
trafficSignal2()
trafficSignal3()
trafficSignal4()

function AutoRefresh(t) {
    setTimeout("location.reload(true)",t)
}